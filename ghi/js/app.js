function createCard(name, venue, description, pictureUrl, startDate, endDate) {
  return `
    <div class="card shadow-lg my-4">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-header">
            <h5 class="card-title">${name}</h5>
            <p class="card-subtitle text-secondary">${venue}<p>
        </div>
        <div class="card-body">
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            <p>
                ${startDate} - ${endDate}
            <p>
        </div>
    </div>
    `;
}

function createAlert(error) {
  return `
        <div class="alert alert-warning" role="alert">
            ${error}
        </div>
    `;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";
  try {
    const response = await fetch(url);

    if (!response.ok) {
      console.log("Must be promise object.");
    } else {
      const data = await response.json();
      let colNum = 1;
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          //   const details = await detailResponse.json();
          //   const detail = details.conference;
          //   const descriptionTag = document.querySelector(".card-text");
          //   descriptionTag.innerHTML = detail.description;
          //   const imageTag = document.querySelector(".card-img-top");
          //   imageTag.src = detail.location.picture_url;
          //   const html = createCard(title, description, pictureUrl);
          const details = await detailResponse.json();
          const title = details.conference.name;
          const venue = details.conference.location.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date(
            details.conference.starts
          ).toLocaleDateString();
          const endDate = new Date(
            details.conference.ends
          ).toLocaleDateString();
          const html = createCard(
            title,
            venue,
            description,
            pictureUrl,
            startDate,
            endDate
          );
          let column = document.querySelector(`#colNum${colNum}`);
          column.innerHTML += html;
          colNum++;
          if (colNum === 4) {
            colNum = 1;
          }
        }
      }
    }
  } catch (err) {
    const error = err.message;
    let html = createAlert(error);
    const main = document.querySelector("#main");
    main.innerHTML = html;
  }
});
